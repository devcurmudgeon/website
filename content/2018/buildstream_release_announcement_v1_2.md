Title: BuildStream 1.2 is out!
Date: 2018-09-05 12:00 
Category: release
Tags: 1.2, 
Slug: BuildStream-1.2-is-out!
Authors: toscalix
Summary: BuildStream 1.2 has been released with new features and siginificant improvements.


##### *The BuildStream project releases the first mature version of the integration toolset,* **[BuildStream 1.2](https://buildstream.build/download.html).**

The BuildStream project is happy to announce the release of the latest version of its integration toolset, **[BuildStream 1.2](https://buildstream.build/download.html)**. This version represents a remarkable step forward with the inclusion of several new features required in production environments. Almost every previously available feature has been improved and many bugs have been fix during the development cycle that led to this 1.2 release.

Among the most relevant news we highlight a new artifact cache based on CAS, that significantly speed up build times, a new structure of the tool's configuration file which together with new capabilities to fetch from multiple sources increase BuildStream flexibility and various minor features and improvement that provides a better onboarding experience to those using BuildStream for the very first time as well as a raise in productivity to BuildStream veterans.

In order to now more about this new version, please visit the **[BuildStream 1.2 Feature](https://buildstream.build/feature.html)** page.

## Who is BuildStream for?

BuildStream aims to be the default integration solution for integrators of complex systems and applications that will be in production for a long time, which requires that the toolset is not just powerful, scalable, flexible and efficient, among other qualities, but also reliable and cost effective in the mid term. The project also envision to meet strict requirements coming from safety critical environments.

## About BuildStream

BuildStream is a Free Software project. You can learn more about what it is by visiting the [Frequently Asked Questions](https://buildstream.build/faq.html#about-buildstream) page and the [BuildStream Project](https://buildstream.build/community.html) page. If you are interested in learning more about what BuildStream delivers, please visit our [BuildStream Portfolio](https://buildstream.build/portfolio.html) page.
