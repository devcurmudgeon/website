title: Community
save_as: community.html

<!-- This is the so called project page  -->

[TOC]


Paragraph describing BuildStream as an Open Source project.

# BuildStream Manifest

Paragraph describing BuildStream principles, mission and target audiences (users and contributors)

# BuildStream Governance

This section describes the governance aspects of the project, including the licenses (link to the licensce page of the buildstream repo) and the project sponsors, as well as the relation with GNOME.

# Participate in the BuildStream project

This section includes the information required to participate in the project including:
* Consume BuildStream (download page)
* Report bugs:  link to FAQ where this question is answered. Include context.
* Test the latest features: context and link to the development snapshots in the download page.
* Develop BuildStream
   * You should be entitled to the principles and general governance rules. Accept the license. We use gitlab. No CLA and No CoO.
   * Link to Master
   * Review code: link to MR list in BuildStream repo
* Support other users or any othet type of contribution:
   * Communication channels and through (provide context)
   * Wiki front page (provide context)
