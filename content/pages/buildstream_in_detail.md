title: BuildStream in Detail
save_as: detail.html

[TOC]


# BuildStream overview

<!-- This where we can define buildstream with technical words, for experts. Those who reach this point have already installed BuildStream. This paragraph should highlight the strengths.   -->


# BuildStream description
<!-- Showrt sentence to provide context to what is coming   -->

## Block diagram
<!-- Draw the BuildStream block diagram and include it here. Add the legen below the block diagram. Remember to add before the block diagram the version which correspond with the diagram. we will need to update it.   -->

## Block diagram components
<!-- This is the section where you briefly describe each component present in the block diagram. In the future we will create a page for each one of them if required. If there is an explanation on the documentation, link it here but always provide context.   -->


# BuildStream main features
<!-- In the feature page of each release, there is a table including the new features and those who has been updated. Of that table there will be a link to each feature section -->
## Feature 1
<!-- Technical description of the feature 1. Add why this feature is important. Compare it with other projects highlighting why it is such a differentiaion factor, or why not -->
## Feature 2
<!-- Technical description of the feature 1. Add why this feature is important. Compare it with other projects highlighting why it is such a differentiaion factor, or why not -->
## Feature 3
<!-- Technical description of the feature 1. Add why this feature is important. Compare it with other projects highlighting why it is such a differentiaion factor, or why not -->


# How to use BuildStream: examples
<!-- Once the user has installed BuildStream and read about the cool features, the next step is...what can I do with the tool? Create a paragraph here that explain what are the examples for and why should they follow/try them.   -->
## Example 1
<!-- Shortescritpion of the example, to provide context. Why should I try it? Who is for? Link to the instructions   -->
## Example 2
<!-- Shortescritpion of the example, to provide context. Why should I try it? Who is for? Link to the instructions   -->
## Example 3
<!-- Shortescritpion of the example, to provide context. Why should I try it? Who is for? Link to the instructions   -->
## Build Flatpak images with BuildStream

<!-- Provide a couple of sentences for context and point to the link: https://gitlab.com/freedesktop-sdk/freedesktop-sdk/wikis/Building-flatpak-images-with-buildstream   -->



# Adapt BuildStream to your needs
<!-- Section focused in turning users into contributors. Add a paragraph about how important is to contribute in order to shape the project and adapt it for your needs. Provide a line of context to each link.  -->

* Link to the Contributors section of the FAQ 
* Link to how to contribute section of the project page.
* Links to the code (gitlab repo)
* Link to documentation section for contributors
