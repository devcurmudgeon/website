title: Frequently Asked Questions
save_as: faq.html

[TOC]

## About BuildStream

A.1.- What is BuildStream?

* **The Free Software project**: BuildStream is a Free Software project hosted by the [GNOME Foundation](https://www.gnome.org/foundation/) focused on improving the continuous integration of complex systems and applications. The project aim to pay special attention to those developers and integrators who care about the maintainability of their projects during a long period of time. 
* **BuildStream, the toolset**: BuildStream is a powerful and flexible software integration toolset. It has been designed to create different outputs out of a unique input and, at the same time, it is able to adapt to complex workflows, even when other build tools are required. An important part of BuildStream is a sister project called BuildGrid, that allows BuildStream to build at scale.
* **BuildStream, the Gitlab Group**: the BuildStream project uses [Gitlab.com](https://gitlab.com) service to host the repositories and manage the project. To do so, a [Group called BuildStream](https://gitlab.com/BuildStream) was created.
* **buildstream, the code repository**: within the BuildStream Group on [Gitlab.com](https://gitlab.com/BuildStream) you can find a repository called [buildstream](https://gitlab.com/BuildStream/buildstream) where the most important code is hosted.

A.2.- Who is BuildStream tool-set for?


A.3.- Why should I use BuildStream?


A.4.- Which license is BuildStream using?

BuildStream project selected the [LGPLv2.1](https://gitlab.com/BuildStream/buildstream/blob/master/COPYING) license for the code and [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

The syster project BuildGrid, which is not part of the GNOME Foundation, is published with [Apache v2.0](https://gitlab.com/BuildGrid/buildgrid/blob/master/LICENSE).


A.5.- Who is developing BuildStream?


## Use BuildStream

U.1.- I want to give BuildStream a try. Where can I get it?

U.2.- Which BuildStream version should I try?

U.3.- How do I install BuildStream?

U.4.- I need help with BuildStream installation, where can I get it?

U.5.- I have BuildStream installed, now what?


## Contribute to BuildStream

C.1.- I found a bug, how can I report it?

C.2.- Where can I get the development version of BuildStream?

C.3.- How do I install the development version of BuildStream?

C.4.- I would like to submit a patch, what should I do?

C.5.- I have a request to improve BuildStream, how can I present it to the BuildStream community?


