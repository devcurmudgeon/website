title: Glossary
save_as: glossary.html

<!-- Glossary page. Check the content structure to better understand the relation with other pages: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/content_design/content_structure_proposal_description.md#buildstream-in-detail -->

## BuildStream Glossary

[TOC]

### B

* build: <!-- vs integration -->
* BuildStream: <!-- the tool -->
* buildstream: <!-- the repo -->
* BuildStream Project: 
* BuildStream Community

### D

* Definitions: <!-- vs receipe -->

### I

* Integration: <!-- vs build -->

### M

* Master:

### P

* Pipeline: 

### R 

* Recipe:

### W

* Workspace: 
