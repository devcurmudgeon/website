title: Known Issues
save_as: known-issues.html

<!-- Known issues page page. Check the content structure to better understand the relation with other pages: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/content_design/content_structure_proposal_description.md#buildstream-in-detail -->

<!-- This page targets users, so they do not need to be familiar with the insides of the tool. Add a description about the impact of the bug, not about the technical details. The title does not need to match the bug one -->

<!-- Known issues should include in the ticket the workaround since we will route users to it -->

## Known issues

This page lists the issues that has been identified by the BuildStream team or by the community that might affect you in early stages of the BuildStream usage or that might have a significnat impact in certain actions. When available, a workaround is offered. In any case, the BuildStream community is working to fix them.

### BuildStream 1.2

* #XXX (Title) <!-- The title does not need to match the bug one. It needs to be easy to identify by its impact.  -->
   * (Description): <!-- Add a description about the impact of the bug, not about the technical details. Remember to include the workaround on the ticket description. -->
* #XXX (Title) <!-- The title does not need to match the bug one. It needs to be easy to identify by its impact.  -->
   * (Description): <!-- Add a description about the impact of the bug, not about the technical details. Remember to include the workaround on the ticket description. -->
* #XXX (Title) <!-- The title does not need to match the bug one. It needs to be easy to identify by its impact.  -->
   * (Description): <!-- Add a description about the impact of the bug, not about the technical details. Remember to include the workaround on the ticket description. -->
* #XXX (Title) <!-- The title does not need to match the bug one. It needs to be easy to identify by its impact.  -->
   * (Description): <!-- Add a description about the impact of the bug, not about the technical details. Remember to include the workaround on the ticket description. -->
