title: Download
save_as: download.html

[TOC]

<!-- Tables in this page are generated with get_release.py -->

# Releases

Latest version:
<object style="vertical-align: middle" data="https://buildstream.gitlab.io/buildstream/_static/release.svg" type="image/svg+xml">
(your browser does not support SVG, please find releases at [https://download.gnome.org/sources/BuildStream/](https://download.gnome.org/sources/BuildStream/).
</object>

| Version | URL | SHA256 | Changes |
|:-------:|:---:|:------:|:-------:|
_download_table_stable:| {version} | [{basename}]({uri}) | {sha256} | [{news-basename}]({news}) |

# Development snapshots

Latest version:
<object style="vertical-align: middle" data="https://buildstream.gitlab.io/buildstream/_static/snapshot.svg" type="image/svg+xml">
(your browser does not support SVG, please find releases at [https://download.gnome.org/sources/BuildStream/](https://download.gnome.org/sources/BuildStream/).
</object>

| Version | URL | SHA256 | Changes |
|:-------:|:---:|:------:|:-------:|
_download_table_unstable:| {version} | [{basename}]({uri}) | {sha256} | [{news-basename}]({news}) |

| Version      | OS           | Download & MD5 sum       | Instructions |  Artifacts |
| :----------: | :------:     | :----------------------: | :----------: |  :----------: |
|              |              | Debian (.deb) Checksum xxxxx | Please read the following considerations before installing the .deb package  | xxxxxxxx |
| v1.2 (link to release announcement)         | Linux        | Fedora (.rpm) Checksum xxxxx | Please read the following considerations before installing the .rpm package | xxxxxxxxx |
|              |              | pip           Checksum xxxxx | Download instructions. Installation instructions  | xxxxxxxxx |
|              | MacOS/Windows|   No supported yet           |                                                   |           |
| Cell 1       | Cell 2   | Cell 3             | Cell 4       |
| Cell 7       | Cell 8   | Cell 9             | Cell 10      |

