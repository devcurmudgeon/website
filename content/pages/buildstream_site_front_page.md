title: Welcome to the BuildStream project
save_as: index.html

BuildStream is a Free Software project that delivers an homonym toolset for integrating software stacks.

The BuildsStream integration toolset targets application developers and system integrators who create production ready software at scale using modern methodologies, that need to be maintained for long periods of time while keeping high levels of efficiency and flexibility.

New to BuildStream? Find out more in our [Frequently Asked Questions](https://buildstream.build/faq.html) page.
